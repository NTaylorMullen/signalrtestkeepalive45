﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;

namespace SignalRTest45
{
    public class extraHub : Hub
    {
        public override System.Threading.Tasks.Task OnConnected()
        {
            return Clients.All.joined(Context.ConnectionId);
        }

        public override System.Threading.Tasks.Task OnDisconnected()
        {
            return Clients.All.left(Context.ConnectionId);
        }

        public override System.Threading.Tasks.Task OnReconnected()
        {
            return Clients.All.rejoined(Context.ConnectionId);
        }
    }
}